# Swap file
Service {
  require => Class['swap::config']
}
class { 'swap::config': }
# Firewall
Firewall {
  before  => Class['my_fw::post'],
  require => Class['my_fw::pre'],
}
class { ['my_fw::pre', 'my_fw::post']: }
class { 'firewall': }
firewall { '100 allow http and https access':
    dport  => [80, 443],
    proto  => tcp,
    action => accept,
}
# Pulp
Yumrepo <| |> -> Package <| |>
yumrepo { 'pulp-2-stable':
  baseurl  => 'https://repos.fedorapeople.org/repos/pulp/pulp/stable/2/$releasever/$basearch/',
  descr    => 'Pulp 2 Production Releases',
  gpgkey   => 'https://repos.fedorapeople.org/repos/pulp/pulp/GPG-RPM-KEY-pulp-2',
  gpgcheck => true,
}
class { 'epel': }
class { 'pulp':
  enable_puppet    => true,
  default_password => 'admin',
}
class { 'pulp::admin':
  enable_puppet => true,
  verify_ssl    => false,
}
file { '/root/.pulp/admin.conf':
  content => "[auth]\nusername: admin\npassword: admin\n",
  mode    => '0400',
}
exec { 'pulp-login':
  command => '/bin/pulp-admin login -u admin -p admin',
  require => [Class['pulp::admin'],File['/root/.pulp/admin.conf']],
}
