# vagrant-pulp
A Vagrant development environment for [Pulp](http://http://www.pulpproject.org/)

## Usage

```bash
vagrant up
```

## Notes
- VM host name is `pulp.test`
- Install the `vagrant-hostmanager` plugin to resolve this host name.
- Pulp is memory hungry.  I've configured the VirtualBox VM provider for 2GB. If
you are using a different hypervisor, you may need to edit the `Vagrantfile`.
